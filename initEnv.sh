#!/bin/bash
# Drop Schema
vendor/bin/doctrine orm:schema-tool:drop --force
# Create Schema from Entities
vendor/bin/doctrine orm:schema-tool:create
# Load Test Data
php loadTestData.php
