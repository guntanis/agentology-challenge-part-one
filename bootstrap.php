<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"), $isDevMode, null, null, false);

$conn = array(
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . '/db.sqlite',
);

/*
 * Uncomment for MySQL backend.
$conn = array(
    'driver' => 'pdo_mysql',
    'url' => 'mysql://user:password@127.0.0.1/agentology'
);
 */

// Obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);
