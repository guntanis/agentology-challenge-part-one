# Agentology Code Challenge | Part One
### By: Jorge Guntanis -  jorge@guntanis.com
## Intro

Thank you for giving me the opportunity to take this challenge. It probably seems pretty straightforward for someone with a bit more context on the architecture and especially the challenges that Agentology has today. 
Without having a lot of context, it was a bit difficult to figure out exactly what was needed out of it, so I took the liberty to implement the architecture in code and from there we will be discussing the pros and cons of the various alternatives.
I feel this will also give you a bit of a window on my coding style and general skills, which I consider important.

## Environment

This should be pretty straightforward, all the necessary code is in this repo, you should not need to run composer, but if it fails, just run *composer install* and that should take care of setting up the Doctrine and Autoload dependencies.

You need PHP 7 with SQLite PDO driver.

For simplicity, I'm using SQLite as the DB backend, but it can be easily modifyable to any other DB engine supported by Doctrine by editing the *boostrap.php* file.
There are a few relevant files that we will be working with:

 - **initEnv.sh**: This takes care of generating the DB Schema and feeding random sample data into it.
 - **loadTestData.php**: This will automatically be ran with initEnv.sh, but you can run it as many times as you want, if you wish to have more sample data. This script will create:
	 - 1 Agent with
		 - 1 Concierge
		 - 109 Leads
		 - 5 Calls
		 - 15 Messages
		 - 4 Emails
		 - 12 Notes
		 - 1 Scheduled Lead
 - **getLeadInfo.php**: This script would be akin to an API call, it uses the exact example DQL query given in the challenge. It takes one argument, *leadId*, which should be a number.
 
## Setting up the Environment
 Clone this repo into a local folder.
 Run:

    # bash initEnv.sh
Done.
## Usage

    getLeadInfo.php gets all the relational information from a lead.
    Usage:
       php getLeadInfo.php <leadId>

    php getLeadInfo.php 10
    array(9) {
      ["id"]=> 
      int(10)
      ["name"]=>
      string(23) "Emilie Jones 1532826145"
      ["agent"]=>
	  array(2) {
		["id"]=>
	    int(1)
	    ["name"]=>
	    string(25) "Jorge Guntanis 1532826144"`	
	 }
	 ["concierge"]=>
	 ...
	 ...
    
## Data Model
![Data Model](https://guntanis.com/diagram.png)

## getLeadInfo.php - Challenge Notes
The query in the example will adequately pull all the relational information from a lead. But there are several issues that we need to consider.

- **Practicality**: would we ever need all this information in the same place? Depending on the use case and the user experience/interface, there is only a subset of all this data that we will ever need. There is no point on creating the overhead that joining all this tables will have.
- **Security**: If this data is going to be exposed to agents, concierges or even third party consumers, it is not wise to just do a "*SELECT \**" on all the tables. One example of why this is a bad idea happened to *Uber* a few years ago, back when it was against their policy to disclose the customer's rating. Their customer facing UI would call an API that would essentially perform a "*SELECT \**" on the Customer. Eventually someone caught up on that and disclosed it. It was an avoidable and silly mistake that cost them some bad publicity. [[1](https://medium.com/@merrypuck/how-to-find-your-uber-passenger-rating-4aa1d9cc927f)]
- **Performance**: Some of the One to Many relationships can/will grow exponentially. The scenario given in the challenge seems a bit optimistic. Realistically, I think we would be looking at hundreds of messages and other interactions, per lead. This makes it impractical to grab all the data in a single query and we will need to use pagination to get some of this data in an efficient manner. This goes hand in hand with practicality, we will probably want specialized queries to handle these historical data with more granularity, which may include filtering (date/time filtering for example) and pagination. Such specialized queries will allow us to also *Lazy Load* some of the data in a way that might make more sense depending on the UI and Use Case.
- **ORM Capabilities**: Part of the beauty of using the ORM is so the queries are written in code, instead of using a proprietary query language. While using DQL makes it a lot easier to read for someone that's familiar with SQL, it would be a better practice to use the ORM's query builder, this will make the creation of more complex, dynamic queries a bit easier. Once we create more specific queries, as suggested above, we would probably want to use the [Repository](https://martinfowler.com/eaaCatalog/repository.html) design pattern that *Doctrine* offers. When extending the *EntityRepository* class we can create custom, more specific *DQL* that we can then reuse throughout the platform. 
