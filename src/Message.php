<?php
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="MessageRepository")
 * @ORM\Table(name="Messages")
 **/
class Message
{
    /** 
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue 
     **/
    private $id;

    /** 
     * @ORM\Column(type="string") 
     **/
    private $message;

    /**
     * @ManyToOne(targetEntity="Lead", inversedBy="messages")
     */
    private $lead;


    public function getId()
    {
        return $this->id;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setLead($lead)
    {
        $lead->messages($this);
        $this->lead = $lead;
    }

    public function getLead()
    {
        return $this->lead;
    }


}
