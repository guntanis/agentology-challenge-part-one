<?php
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @ORM\Entity(repositoryClass="ScheduledLeadRepository")
 * @ORM\Table(name="ScheduledLeads")
 **/
class ScheduledLead
{
    /** 
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue 
     **/
    private $id;

    /** 
     * @ORM\Column(type="string") 
     **/
    private $automation;

    private $lead;


    public function getId()
    {
        return $this->id;
    }

    public function getAutomation()
    {
        return $this->automation;
    }

    public function setAutomation($automation)
    {
        $this->automation = $automation;
    }

    public function setLead($lead)
    {
        $lead->scheduledLead($this);
        $this->lead = $lead;
    }

    public function getLead()
    {
        return $this->lead;
    }

}
