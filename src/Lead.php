<?php
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @ORM\Entity(repositoryClass="LeadRepository")
 * @ORM\Table(name="Leads")
 **/
class Lead
{
    /** 
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue 
     **/
    private $id;

    /** 
     * @ORM\Column(type="string") 
     **/
    private $name;

    /**
     * @ManyToOne(targetEntity="Agent", inversedBy="assignedLeads")
     */
    private $agent;

    /**
     * @ManyToOne(targetEntity="Concierge", inversedBy="assignedConcierge")
     */
    private $concierge;

    /**
     * @OneToMany(targetEntity="Call", mappedBy="lead")
     */
    private $calls = null;

    /**
     * @OneToMany(targetEntity="Message", mappedBy="lead")
     */
    private $messages = null;

    /**
     * @OneToMany(targetEntity="Email", mappedBy="lead")
     */
    private $emails = null;

    /**
     * @OneToMany(targetEntity="Note", mappedBy="lead")
     */
    private $notes = null;

    /**
     * @OneToOne(targetEntity="ScheduledLead")
     */
    private $scheduledLead;

    public function __construct()
    {
        $this->calls = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAgent($agent)
    {
        $agent->assignedToLead($this);
        $this->agent = $agent;
    }

    public function getAgent()
    {
        return $this->agent;
    }

    public function setConcierge($concierge)
    {
        $concierge->assignedToLead($this);
        $this->concierge = $concierge;
    }

    public function getConcierge()
    {
        return $this->concierge;
    }

    public function calls($call)
    {
        $this->calls[] = $call;
    }

    public function messages($message)
    {
        $this->messages[] = $message;
    }

    public function emails($email)
    {
        $this->emails[] = $email;
    }

    public function notes($note)
    {
        $this->notes[] = $note;
    }

    public function scheduledLead($scheduledLead)
    {
        $this->scheduledLead = $scheduledLead;
    }

}
