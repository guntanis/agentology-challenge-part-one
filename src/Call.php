<?php
// src/Calls.php
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity
 * @ORM\Table(name="Calls")
 **/
class Call
{
    /** 
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue 
     **/
    private $id;

    /** 
     * @ORM\Column(type="string") 
     **/
    private $fromNumber;

    /**
     * @ManyToOne(targetEntity="Lead", inversedBy="calls")
     */
    private $lead;


    public function getId()
    {
        return $this->id;
    }

    public function getFromNumber()
    {
        return $this->fromNumber;
    }

    public function setFromNumber($fromNumber)
    {
        $this->fromNumber = $fromNumber;
    }

    public function setLead($lead)
    {
        $lead->calls($this);
        $this->lead = $lead;
    }

    public function getLead()
    {
        return $this->lead;
    }


}
