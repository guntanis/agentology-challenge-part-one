<?php
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity
 * @ORM\Table(name="Emails")
 **/
class Email
{
    /** 
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue 
     **/
    private $id;

    /** 
     * @ORM\Column(type="string") 
     **/
    private $fromEmail;

    /**
     * @ManyToOne(targetEntity="Lead", inversedBy="emails")
     */
    private $lead;


    public function getId()
    {
        return $this->id;
    }

    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    }

    public function setLead($lead)
    {
        $lead->emails($this);
        $this->lead = $lead;
    }

    public function getLead()
    {
        return $this->lead;
    }


}
