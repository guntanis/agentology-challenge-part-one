<?php
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="NoteRepository")
 * @ORM\Table(name="Notes")
 **/
class Note
{
    /** 
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue 
     **/
    private $id;

    /** 
     * @ORM\Column(type="string") 
     **/
    private $note;

    /**
     * @ManyToOne(targetEntity="Lead", inversedBy="notes")
     */
    private $lead;


    public function getId()
    {
        return $this->id;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function setLead($lead)
    {
        $lead->notes($this);
        $this->lead = $lead;
    }

    public function getLead()
    {
        return $this->lead;
    }


}
