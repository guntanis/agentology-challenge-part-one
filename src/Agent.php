<?php
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Entity 
 * @ORM\Table(name="Agents")
 **/
class Agent
{
    /** 
     * @ORM\Id @ORM\Column(type="integer") 
     * @ORM\GeneratedValue 
     **/
    private $id;

    /** 
     * @ORM\Column(type="string") 
     **/
    private $name;

    /**
     * @OneToMany(targetEntity="Lead", mappedBy="agent")
     */
    private $assignedLeads = null;

    public function __construct()
    {
		$this->assignedLeads = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function assignedToLead($lead)
    {
        $this->assignedLeads[] = $lead;
    }

}
