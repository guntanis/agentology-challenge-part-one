<?php
require_once "bootstrap.php";
$leadClass = "Lead";

// Check that there is a leadId and that's a number
if((!isset($argv[1]))||(!is_numeric($argv[1]))) {
    echo "getLeadInfo.php gets all the relational information from a lead.\n";
    echo "Usage:\n  ";
    echo "php getLeadInfo.php <leadId>\n";
    return;
}

$leadId = $argv[1];

// Doctrine Query
$dql = "
SELECT l, a, co, c, m, e, n, s
FROM {$leadClass} l
JOIN l.agent a
LEFT JOIN l.concierge co
LEFT JOIN l.calls c
LEFT JOIN l.messages m
LEFT JOIN l.emails e
LEFT JOIN l.notes n
LEFT JOIN l.scheduledLead s
WHERE l.id = :id
";

// Create SQL Query
$query = $entityManager->createQuery($dql)->setParameter('id', $leadId) ;

try {
    // Get Query Results as Array
    var_dump($query->getArrayResult()[0]);
} catch (Exception $e) {
        echo 'Exception: ',  $e->getMessage(), "\n";
}


