<?php
// create_agent.php <name>
require_once "bootstrap.php";

// Create New Agent
$newAgentName = "Jorge Guntanis ".time();
$agent = new Agent();
$agent->setName($newAgentName);
$entityManager->persist($agent);
$entityManager->flush();
$agentId = $agent->getId();
echo "Created Agent with ID " . $agent->getId() . "\n";

// Create New Concierge
$newConciergeName = "Erika Schultz ".time();
$concierge = new Concierge();
$concierge->setName($newConciergeName);
$entityManager->persist($concierge);
$entityManager->flush();
$conciergeId = $concierge->getId();
echo "Created Concierge with ID " . $concierge->getId() . "\n";

// Create Sample Leads
for($l=0;$l<20;$l++) {
    $newLeadName = "Emilie Jones ".time();
    $lead = new Lead();
    $lead->setName($newLeadName);
    $lead->setAgent($agent);
    $lead->setConcierge($concierge);
    $entityManager->persist($lead);
    $entityManager->flush();
    echo "Created Lead with ID " . $lead->getId() . "\n";
    // Create Sample Calls
    for($c=0;$c<5;$c++) {
		$newFromNumber = '1'.substr(str_shuffle('12345678912345678912346789'),0,10);
		$call = new Call();
		$call->setFromNumber($newFromNumber);
		$call->setLead($lead);
		$entityManager->persist($call);
		$entityManager->flush();
		echo "Created Call with ID " . $call->getId() . "\n";
    }
    // Create Sample Messages
    for($m=0;$m<15;$m++) {
		// Generate Random Message Content
		$newMessage = generateRandomSentence();
		// Persist Message
		$message = new Message();
		$message->setMessage($newMessage);
		$message->setLead($lead);
		$entityManager->persist($message);
		$entityManager->flush();
		echo "Created Message with ID " . $message->getId() . "\n";
    }
    // Create Sample Emails
    for($e=0;$e<4;$e++) {
        $newFromEmail = 'address_'.microtime(true).'@example.com';
		$email = new Email();
		$email->setFromEmail($newFromEmail);
		$email->setLead($lead);
		$entityManager->persist($email);
		$entityManager->flush();
		echo "Created Email with ID " . $email->getId() . "\n";
    }
    // Create Sample Notes
    for($m=0;$m<12;$m++) {
        // Generate Random Note Content
        $newNote = generateRandomSentence();
        // Persist Note
        $note = new Note();
        $note->setNote($newNote);
        $note->setLead($lead);
        $entityManager->persist($note);
        $entityManager->flush();
        echo "Created Note with ID " . $note->getId() . "\n";
    }
    // Schedule an Automation for this Lead
    $automations = array("Email", "SMS", "Call", "SnailMail");
    $scheduledLead = new ScheduledLead();
    $scheduledLead->setAutomation($automations[rand(0,(count($automations)-1))]);
    $scheduledLead->setLead($lead);
    $entityManager->persist($scheduledLead);
    $entityManager->flush();
    echo "Created ScheduledLead with ID " . $scheduledLead->getId() . "\n";

}

// Utility function to generate random sentences based on Lorem Ipsum
function generateRandomSentence()
{
    $words = "ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
    $wordArray = explode(" ", $words);
    shuffle($wordArray);
    $wordCount = rand(5,15);
    $sentenceArray = array_slice($wordArray,0,$wordCount);
    $sentence = implode(" ", $sentenceArray);
    return $sentence;
}
